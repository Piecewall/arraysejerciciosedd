/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

import java.util.Scanner;

/**
 *
 * @author Orlando
 */
public class Arrays {

   
    
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        System.out.println("Orlando Cach 4B 57055");
        System.out.println("");
        
        String[] compas = {"Yazuri", "Ricardo", "Rodrigo", "Cristian", "Josue"};
        int[] edad = {20, 20, 20, 21, 20};
        String[] compas2 = {"William", "Alexander", "Rodrigo", "Cristian", "Gustavo"};
        int[] edad2 = {21, 20, 20, 21, 20};
        double[] edades = {21, 20, 20, 21 ,20};
        double tEdades= 0;
        double promedioEdades = 0;
        double suma = 0;
        double[] nums4 = {1,2,3,4,5,6,7,8,9,10};
        double suma4 = 0;
        double promedio4 = 0;
        String[] día = {"Domingo", "Lunes", "Martes", 
                           "Miércoles", "Jueves", "Viernes",
                           "Sábado"};
        int num5 = 0;
        String PSW [][] = new String [4][4];
        PSW [0][0]= "Luke Skywalker";
        PSW [0][1]= "R2-D2";
        PSW [0][2]= "C-3PO";
        PSW [0][3]= "Darth Vader";
        PSW [1][0]= "Leia Organa";
        PSW [1][1]= "Owen Lars";
        PSW [1][2]= "Beru Whitesun Lars";
        PSW [1][3]= "R5-D4";
        PSW [2][0]= "Biggs Darklighter";
        PSW [2][1]= "Obi-Wan Kenobi";
        PSW [2][2]= "Yoda";
        PSW [2][3]= "Jek Tono Porkins";
        PSW [3][0]= "Jabba Desilijic Tiure";
        PSW [3][1]= "Han Solo";
        PSW [3][2]= "Chewbacca";
        PSW [3][3]= "Anakin Skywalker";
        String[] código = {"001-Calkini", "002-Campeche", "003-Carmen", 
                           "004-Champoton", "005-Hecelchakan", "006-Hopelchen",
                           "007-Palizada", "008-Tenabo", "009-Escarcega",
                           "010-Calakmul", "011-Candelaria", "012-Seybaplaya"};
        
        String[] cabeceras = {"Calkini", "San Francisco de Campeche", "Ciudad del Carmen",
                              "Champoton", "Hecelchakan", "Hopelchen", "Palizada",
                              "Tenabo", "Escarcega", "Xpujil", "Candelaria",
                              "Seybaplaya"};
        int[] habitantes = {52890, 259005, 221094, 83021, 28306, 37777,
                            8352, 10665, 54184, 26882, 41194, 15420};
        int[] viii = new int[]{46,30,78,23,34};
        String[] mes = {"Enero", "Febrero", "Marzo", 
                           "Abril", "Mayo", "Junio",
                           "Julio","Agosto","Septiembre",
                           "Octubre","Noviembre","Diciembre"};
        int num9 = 0;
         int[] Arr1 = {1,2,3,4,5,6,7};            //Arreglo 1 "Maaaambo"            
         int[] Arr2 = {8,6,33,100};               //Arreglo 2 "Aquí no se baila Mambo !!!"
         int[] Arr3 = {2,55,37,91,65};            //Arreglo 3 "Maaaambo"
        
        System.out.println("Primer ejercicio"); 
        Arrays.ej1(compas, edad);
        System.out.println("");
        System.out.println("Segundo ejercicio");
        Arrays.ej2(compas2,edad2);
        System.out.println("");
        System.out.println("Tercer ejercicio");
        Arrays.ej3(edades,tEdades,promedioEdades,suma);
        System.out.println("");
        System.out.println("Cuarto ejercicio");
        Arrays.ej4(nums4,suma4,promedio4);
        System.out.println("");
        System.out.println("Quinto ejercicio");
        Arrays.ej5(día, num5);
        System.out.println("");
        System.out.println("Sexto ejercicio");
        Arrays.ej6(PSW);
        System.out.println("");
        System.out.println("Septimo ejercicio");
        Arrays.ej7(código, cabeceras, habitantes);
        System.out.println("");
        System.out.println("Octavo ejercicio");
        Arrays.ej8(viii);
        System.out.println("");
        System.out.println("Noveno ejercicio");
        Arrays.ej9(mes, num9);
        System.out.println("");
        System.out.println("Decimo ejercicio");
        System.out.println("Arreglo 1");           
        ej10(Arr1);                     //Utiliza para verficar si cuenta con el 7
         
        System.out.println("\nArreglo 2");
        ej10(Arr2);                     //Utiliza para verficar si cuenta con el 7
         
        System.out.println("\nArreglo 3");
        ej10(Arr3);
        System.out.println("");
    }
    public static void ej1(String[] compas, int[] edad){
    for(int i=0;i<compas.length;i++){
    System.out.println(compas[i]+" = "+edad[i]);   
    }
    }
    public static void ej2(String[] compas2, int[] edad2){
    for(int i=0;i<compas2.length;i++){
    System.out.println(compas2[i]+" = "+edad2[i]);   
    }
    }
    public static void ej3(double[] edades,double tEdades, double promedioEdades, double suma){
    for(double i: edades){
        suma += i;}
        for(int i = 0; i < edades.length; i++) {
            tEdades += edades[i];
        }
        promedioEdades = tEdades / edades.length;
        System.out.println("La suma es: " + suma);
        System.out.println("El promedio de las edades es: " + promedioEdades + "años");
    }
    
    public static void ej4(double[] nums4,double suma4, double promedio4){
    for(double i: nums4){
        suma4 += i;
    }
    
    promedio4 = suma4 / nums4.length;
    System.out.println("La suma es: " + suma4);
    System.out.println("El promedio es : " + promedio4);
    }
    public static void ej5(String[] día, int num5){
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número del 1 al 7");
        System.out.println("Nota: La cuenta comienza con Domingo como el número 1");
        num5 = sc.nextInt();
        
        if(num5 < 8 && num5 > 0){
            System.out.println(día[num5 - 1]);
        }else{
            System.out.println("El número " + num5 + " esta fuera del rango");
        }
    }
    public static void ej6(String[][] PSW){
    
    System.out.println("\nPersonajes de Star Wars (ciclo for simple): ");
        for(int i=0;i<4;i++){
        System.out.println(java.util.Arrays.toString(PSW[i]));
        }
        System.out.println("\nPersonajes de Star Wars (ciclo for each): ");
        for(String[] i:PSW){
        System.out.println(java.util.Arrays.toString(i));}
        
    }
    public static void ej7(String[] código, String[] cabeceras, int[] habitantes){
    Scanner sc = new Scanner(System.in);
        
        
        //Inicia parte uno
        for(int i = 0; i < 12; i++){
            System.out.println(código[i] + "   " + cabeceras[i] + "   " + habitantes[i]);
}
        //Finaliza parte uno
        int num;
        int num2;
        
        //Inicia parte dos
        System.out.println("");
        System.out.println("Ingrese un número del 1 al 12");
        num = sc.nextInt();
        
        
        if(num < 13 && num > 0){
            System.out.println("Resultado: "+código[num - 1] + "|" + cabeceras[num -1] + "|" + habitantes[num -1]);
        }else{
            System.out.println("El número " + num + "esta fuera del rango");
        }
        //Finaliza parte dos
        //Inicia parte tres
        System.out.println("");
        System.out.println("Ingrese un número del 1 al 12");
        num2 = sc.nextInt();
        if(num2 > 0 && num2 < 13){
            switch (num2){
                case 1:
                    //desplegar posterior
                     System.out.println("Resultado: "+código[num2] + "|" + cabeceras[num2 -1] + "|" + habitantes[num2 -1]);
                     break;
                case 12:
                    //desplegar el anterior
                    num2--;
                    System.out.println("Resultado: "+código[num2 - 1] + "|" + cabeceras[num2 -1] + "|" + habitantes[num2 -1]);
                    break;
                default:
                    //desplegar el anterior y el posterior
                    num2--;
                    System.out.println("Resultado: "+código[num2 - 1] + "|" + cabeceras[num2 -1] + "|" + habitantes[num2 -1]);
                    num2++;
                     System.out.println("Resultado: "+código[num2] + "|" + cabeceras[num2 -1] + "|" + habitantes[num2 -1]);
                     break;
            }
           
        }else{
            System.out.println("El número " + num2 + "esta fuera del rango");
        }
        //Finaliza parte tres
    
    
    
    
    }
    public static void ej8(int[] viii){
        System.out.println("El primer elemento del array es: "+viii[0]);
        System.out.println("El último elemento del array es: "+viii[viii.length-1]);
  
    }
    
    public static void ej9(String[] mes, int num9){
        Scanner sc = new Scanner(System.in);
        
       
        System.out.println("Ingrese un número del 1 al 12");
        
        num9 = sc.nextInt();
        
        if(num9 < 13 && num9 > 0){
            System.out.println(mes[num9 - 1]);
        }else{
            System.out.println("El mes " + num9 + " no existe");
        }
    
    }
    
    public static void ej10(int[] NombArreglo){
    
     String var = "";                                                   //declaro variable para concatenar
       
        for(int i=0;i<NombArreglo.length;i++){                            //ciclo para leer los arreglos                
           var = var + NombArreglo[i];
               
        }
        
       if(var.indexOf("7")== -1){                                          //metodo para valorar si esta el 7
             System.out.println("Aquí no se baila Mambo !!!"); 
                
            }else{
              
                System.out.println("Maaaambo");
             }    
    
         
    
    }

    }
    

